# alfred-golinksio
An [Alfred](https://www.alfredapp.com) workflow to search and open go/links from [GoLinks.io](https://www.golinks.io).

# Get it
- Download the [latest release](https://gitlab.com/flippidippi/alfred-golinksio/-/tags)

# Help
- `go {{query}}` - search for go/links and follow them
  - Use without query to go to GoLinks.io home page
  - If you know the exact go/link term you can use `Open go/link {{query}}` to skip search
